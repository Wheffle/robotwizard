///ability_flash();
/*
    Designed to be executed in obj_player.
*/
{
    with(hex)
    {
        for (var i = 0; i < ds_list_size(hexlist); i++)
        {
            var hex = hexlist[| i];
            hex_ignite(hex);
        }
    }
}

///id = anim_create(x, y, sprite, image_speed, image_angle);
{
    var i = instance_create(argument0, argument1, obj_anim);
    with(i)
    {
        sprite_index = argument2;
        image_speed = argument3;
        image_angle = argument4;
    }
    return i;
}

///button_autoselect();
{
    with(obj_button)
    {
        button_deselect();
    }
    
    with(obj_button)
    {
        if (auto_select)
        {
            button_select();
            exit;
        }
    }   
}

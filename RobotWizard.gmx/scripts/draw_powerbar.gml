///draw_powerbar(x, y, title_sprite, color, amount, max);
{
    var i = 0;
    var xx = argument0;
    var yy = argument1;
    
    draw_sprite_ext(argument2, 0, xx, yy, 1, 1, 0, argument3, 1);
    yy += 20;
    
    for (i = 2; i <= argument5; i += 2)
    {
        var idx = 0;
        if (argument4 < i-1) idx = 2;
        else if (argument4 < i) idx = 1;
        
        draw_sprite_ext(s_bar, idx, xx, yy, 1, 1, 0, argument3, 1);
        yy += 8;
    }
}

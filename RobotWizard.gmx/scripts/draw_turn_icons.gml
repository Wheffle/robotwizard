///draw_turn_icons(x, y, actions);
{
    if (argument2 > 0)
    {
        draw_sprite(s_turnicon, 0, argument0, argument1);
        draw_set_font(font0);
        draw_set_color(c_white);
        draw_text(argument0+36, argument1+4, "x" + string(argument2));
    }
    else
    {
        draw_sprite(s_turnicon, 1, argument0, argument1);
    }   
}

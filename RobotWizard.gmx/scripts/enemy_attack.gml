///enemy_attack(enemy_id);
with(argument0)
{
    var dir = 0;
    with(obj_player)
    { 
        mp -= other.damage;
        dir = point_direction(other.x, other.y, x, y);
        audio_play_sound(fx_drain, 10, false);
    }
    
    anim_create(x, y, s_drain, 0.5, dir);
    unit_bump(id, dir);
    reveal = 1;
}

///enemy_destroy(enemy_id);
with(argument0)
{
    with(hex)
    {
        var dice = random(1);
        if (dice > 0.50 and !tutorial_active()) powerup = true;
        unit = noone;
    }
    anim_create(x, y, sprite_death, 0.5, 0);
    if (instance_number(obj_enemy) == 1)
    {
        rm_unlock();
    }
    audio_play_sound(fx_hit, 10, false);
    event_user(2);
    instance_destroy();
}

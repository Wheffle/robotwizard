///sprite = get_track_sprite(hexdir);
{
    switch(argument0)
    {
        case hexdir.n:  return s_track_n;
        case hexdir.s:  return s_track_s;
        case hexdir.ne: return s_track_ne;
        case hexdir.sw: return s_track_sw;
        case hexdir.nw: return s_track_nw;
        case hexdir.se: return s_track_se;
    }
    
    return undefined;
}

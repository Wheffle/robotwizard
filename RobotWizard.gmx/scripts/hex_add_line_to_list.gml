///hex_add_line_to_list(hex, list, dir);
{
    with(argument0)
    {
        ds_list_add(argument1, id);
        var h = hexmap[? argument2];
        if (instance_exists(h)) hex_add_line_to_list(h, argument1, argument2);   
    }   
}

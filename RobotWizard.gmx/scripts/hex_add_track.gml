///hex_add_track(hex, dir, [reverse]);
{
    with(argument[0])
    {
        var dir = argument[1];
        if (argument_count > 2 and argument[2]) dir = hexdir_get_reverse(argument[1]);
        var sprite = get_track_sprite(dir);
        
        ds_list_insert(tracklist_unknown, 0, sprite);
        while(ds_list_size(tracklist_unknown) >= max_tracks)
        {
            ds_list_delete(tracklist_unknown, ds_list_size(tracklist_unknown)-1);
        }
    }
}

///bool = hex_check_neighbor(hex, alleged_neighbor);
{
    with(argument0)
    {
        return ds_list_exists(hexlist, argument1);
    }
    
    return undefined;
}

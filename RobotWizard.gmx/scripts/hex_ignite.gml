///hex_ignite(hex);
/*
    Flashes a hex, revealing and stunning enemies on it.
*/
{
    with(argument0)
    {
        //set light level
        light = 1;
        
        //reveal tracks
        ds_list_clear(tracklist);
        ds_list_copy(tracklist, tracklist_unknown);
        
        //enemy ignite
        with(unit) event_user(0);
    }
}

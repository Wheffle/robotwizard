///id = hex_instance_create(hex, obj_index);
/*
    Creates an instance in the center of a hex, passes that hex's id to the instance.
*/
{
    with(argument0)
    {
        var xx = x + half_width;
        var yy = y + half_height;
        var i = instance_create(xx, yy, argument1);
        unit = i;
        i.hex = id;
        return i;
    }
    
    return undefined;
}

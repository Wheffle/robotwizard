///hex_reveal(hex, reveal_time);
/*
    Dimmly flashes a hex, revealing enemies and tracks.
*/
{
    with(argument0)
    {
        //set light level
        light = 0.60;
        
        //reveal tracks
        ds_list_clear(tracklist);
        ds_list_copy(tracklist, tracklist_unknown);
        
        //enemy reveal
        with(unit) reveal = argument1;
    }
}

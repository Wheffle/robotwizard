///hexdir = hexdir_get_reverse(hexdir);
{
    switch(argument0)
    {
        case hexdir.n:  return hexdir.s;
        case hexdir.s:  return hexdir.n;
        case hexdir.ne: return hexdir.sw;
        case hexdir.nw: return hexdir.se;
        case hexdir.se: return hexdir.nw;
        case hexdir.sw: return hexdir.ne;
    }   
    return undefined;
}

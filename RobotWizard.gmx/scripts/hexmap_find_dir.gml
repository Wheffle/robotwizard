///hexdir = hexmap_find_dir(hex, neighbor);
{
    with(argument0)
    {
        var dir = ds_map_find_first(hexmap);
        while(!is_undefined(dir))
        {
            var h = hexmap[? dir];
            if (h == argument1) return dir;
            dir = ds_map_find_next(hexmap, dir);
        }
    }
    
    return undefined;
}

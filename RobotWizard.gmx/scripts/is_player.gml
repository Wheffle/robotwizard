///bool = is_player(instance_id);
{
    with(argument0)
    {
        return (object_index = obj_player);
    }
    
    return undefined;
}

///moveto_hex(hex);
/*
    Starts current instance moving to specified hex.
*/
{  
    moving = true;
    move_x = (argument0).x + (argument0).half_width;
    move_y = (argument0).y + (argument0).half_height;
    
    hex.unit = noone;
    (argument0).unit = id;
    hex = argument0;
}

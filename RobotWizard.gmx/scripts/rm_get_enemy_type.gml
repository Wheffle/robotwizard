///object_index = rm_get_enemy_type();
{
    with(ctrl_game)
    {
        if (room == rm_tutorial) return obj_enemy;
        if (rm < 4) return obj_enemy;
        return choose(obj_enemy, obj_enemy2);
    }
    
    return undefined;
}

///real = rm_get_num_enemies();
{
    with(ctrl_game)
    {
        if (room == rm_tutorial) return 1;
        if (rm < 4) return 1;
        if (rm < 6) return 2;
        if (rm < 9) return 3;
        return 4;
    }
    
    return undefined;
}

///bool = rm_is_locked();
{
    with(ctrl_rm) return room_locked;
    return undefined;
}

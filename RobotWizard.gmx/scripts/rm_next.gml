///rm_next();
{
    with(ctrl_game)
    {
        with(obj_player)
        {
            other.mp = self.mp;
        }
    
        rm++;
        room_goto(choose(rm0, rm1, rm2, rm3, rm4, rm5, rm6));
    }
}

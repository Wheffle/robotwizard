///rm_unlock();
{
    with(ctrl_rm) room_locked = false;
    with(obj_hex)
    {
        hex_ignite(id);
        light_floor = 0.60;
    }
    audio_play_sound(fx_clear, 10, false);
}

///bool = tutorial_button_check(button_object);
{
    with(ctrl_tutorial)
    {
        if (argument0 != tutorial_button[idx]) return false;
    }
    
    return true;
}

///bool = tutorial_hex_check(hex);
{
    with(ctrl_tutorial)
    {
        if (argument0 != tutorial_hex[idx]) return false;
    }
    return true;
}
